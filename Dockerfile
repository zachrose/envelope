FROM ruby:2.0.0
RUN apt-get update && apt-get install -y build-essential libpq-dev

ENV PHANTOMJS_VERSION 1.9.7
RUN wget -O - https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 | tar xjC /opt
RUN ln -s /opt/phantomjs-$PHANTOMJS_VERSION-linux-x86_64/bin/phantomjs /usr/bin/phantomjs

RUN mkdir /kindreply
WORKDIR /kindreply
ADD Gemfile /kindreply/Gemfile
ADD Gemfile.lock /kindreply/Gemfile.lock
RUN bundle install
ADD . /myapp
