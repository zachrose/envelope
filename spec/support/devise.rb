require 'devise'

# The most minimal config possible
Devise.setup do |config|
  require 'devise/orm/active_record'
end