require 'spec_helper'
require 'ext/string'

describe "nice_title" do
  it "makes titleize respect hypens" do
    'man from the boondocks'.nice_title.should == "Man From The Boondocks"
    'x-men: the last stand'.nice_title.should == "X-Men: The Last Stand"
    'TheManWithoutAPast'.nice_title.should == "The Man Without A Past"
    'raiders_of_the_lost_ark'.nice_title.should == "Raiders Of The Lost Ark"
  end
end

describe "pluralize_regular" do
  it "pluralizes a word without regard for irregular plurals" do
    'mouse'.pluralize_regular.should == 'mouses'
  end
  it "obeys special rules for certain endings" do
    # irregular
    'cactus'.pluralize_regular.should == 'cactuses'
    'quiz'.pluralize_regular.should == 'quizes'
    # regular anyways
    'xerox'.pluralize_regular.should == 'xeroxes'
    'church'.pluralize_regular.should == 'churches'
    'bash'.pluralize_regular.should == 'bashes'
  end
  it "is used to pluralize a proper noun" do
    'Mouse'.pluralize_regular.should == 'Mouses'
    'Cherry'.pluralize_regular.should == 'Cherrys'
  end
end
