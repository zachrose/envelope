require 'spec_helper'
require 'main_event'
require 'companion_event'
require 'reply_set'
require 'reply_set_presenter'

describe ReplySetPresenter do
  
  before(:each) do
    @main_event = MainEvent.create short_name: "example-event"
    @companion_event = CompanionEvent.create kind: "Party", info: "Blah blah blah", main_event: @main_event
    @party = Party.create! name: "Example", main_event: @main_event
    @jimi = Guest.create! first_name: "Jimi", party: @party
    @mary = Guest.create! first_name: "Mary", party: @party
    @jose = Guest.create! first_name: "Joe", party: @party
    @jimi.invite_to(@companion_event)
    @mary.invite_to(@companion_event)
    @jose.invite_to(@companion_event)
    @reply_set = ReplySet.new({ guest: @jimi }).initialize_empty_replies
  end
  
  it "is created with a reply_set" do
    presenter = ReplySetPresenter.new(@reply_set)
    presenter.should be
  end
  
  describe ".events" do
    it "returns a list of events" do
      events = ReplySetPresenter.new(@reply_set).events
      events.should have(2).items
      events.should include(@main_event, @companion_event)
    end
  end
  
  # I think I want to keep the I18n inside my presenter, otherwise I have
  # to have a more complicated method call in my view, which I dislike.
  # To test this without mocking I18n, I'm just testing the presenter
  # methods that give the I18n key and guest_sentence.
  # As a consequence, it doesn't matter whether I test coming or not_coming,
  # the translation_key and guest_sentence methods are the same.
  describe "grammar" do
    it "gets the grammar right when just the focused guest is coming" do
      @reply_set.replies.select { |r|
        r.guest == @jimi
      }.each do |r| r.status = true end
      presenter = ReplySetPresenter.new(@reply_set)
      presenter.translation_key(@main_event, :coming).should == 'coming.focus'
    end
    it "gets the grammar right when the focused guest and another guest are coming" do
      @reply_set.replies.select { |r|
        r.guest == @jimi || r.guest == @mary
      }.each do |r| r.status = true end
      presenter = ReplySetPresenter.new(@reply_set)
      presenter.translation_key(@main_event, :coming).should == 'coming.focus_and_guest'
      presenter.guest_sentence(@main_event, :coming).should == "Mary"
    end
    it "gets the grammar right when the focused guest and many guests are coming" do
      @reply_set.replies.select { |r|
        r.guest == @jimi || r.guest == @mary || r.guest = @jose
      }.each do |r| r.status = true end
      presenter = ReplySetPresenter.new(@reply_set)
      presenter.translation_key(@main_event, :coming).should == 'coming.focus_and_guests'
      presenter.guest_sentence(@main_event, :coming).should == "Joe and Mary"
    end
    it "gets the grammar right when a non-focued guest is coming" do
      @reply_set.replies.select { |r|
        r.guest == @mary
      }.each do |r| r.status = true end
      presenter = ReplySetPresenter.new(@reply_set)
      presenter.translation_key(@main_event, :coming).should == 'coming.guest_or_guests'
      presenter.guest_sentence(@main_event, :coming).should == "Mary"
    end
    it "gets the grammar right when many non-focued guests are coming" do
      @reply_set.replies.select { |r|
        r.guest == @mary || r.guest == @jose
      }.each do |r| r.status = true end
      presenter = ReplySetPresenter.new(@reply_set)
      presenter.translation_key(@main_event, :coming).should == 'coming.guest_or_guests'
      presenter.guest_sentence(@main_event, :coming).should == "Joe and Mary"
    end
  end
    
end