require "active_record"
require "database_cleaner"
# require "pry-debugger"

ActiveRecord::Base.establish_connection adapter: "sqlite3", database: ":memory:"


# sqlite3 hates our mysql indexes
ActiveRecord::Migration.class_eval do
  def add_index *; end
  def enable_extension *; end
end

RSpec.configure do |config|
  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    silence_stream(STDOUT) do
      load "db/schema.rb"
    end
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

$LOAD_PATH << "app/models"
$LOAD_PATH << "app/presenters"
$LOAD_PATH << "app/helpers"

Dir["./spec/support/**/*.rb"].sort.each {|f| require f}
