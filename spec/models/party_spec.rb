require 'spec_helper'
require 'party'
require 'guest'

describe Party do
  
  it "can be created with a name" do
    party = Party.create! name: "Kanye West's Entourage"
    party.name.should == "Kanye West's Entourage"
  end
  
  describe "default naming" do
    describe "when there is but one guest" do
      it "will be just the first or last name" do
        @party = Party.create! guests: [
          Guest.new(first_name: "Darth", last_name: "Vader")
        ]
        @party.name.should == "Vader"
        @party = Party.create! guests: [
          Guest.new(first_name: "Snoopy")
        ]
        @party.name.should == "Snoopy"
      end
    end
    describe "when all guests have a common last name" do
      it "will be a plural of the common last name" do
        @party = Party.create! guests: [
          Guest.new(first_name: 'Mickey', last_name: "Mouse"),
          Guest.new(first_name: 'Minnie', last_name: "Mouse")
        ]
        @party.name.should == "The Mouses"
      end
    end
    describe "when last names are varied" do
      it "will to_sentence last names when appropriate" do
        @party = Party.create! guests: [
          Guest.new(last_name: "Zimmer"),
          Guest.new(last_name: "Gunsul"),
          Guest.new(last_name: "Frasca")
        ]
        @party.name.should == "Zimmer, Gunsul, and Frasca"
      end
    end
    describe "when last names are not present" do
      it "will use first names" do
        @party = Party.create! guests: [
          Guest.new(first_name: "Huey"),
          Guest.new(first_name: "Dewey"),
          Guest.new(first_name: "Louie")
        ]
        @party.name.should == "Huey, Dewey, and Louie"
      end
    end
    describe "when the party name is not unique" do
      it "will append a number at the end" do
        @main_event = MainEvent.create! short_name: 'party'
        @irrelevant_party = Party.create!({
          main_event: MainEvent.create!( short_name: 'something-else'),
          guests: [
            Guest.new(first_name: 'Pammy', last_name: "Mouse"),
            Guest.new(first_name: 'Tammy', last_name: "Mouse")
          ]
        })
        @party = Party.create! main_event: @main_event, guests: [
          Guest.new(first_name: 'Mickey', last_name: "Mouse"),
          Guest.new(first_name: 'Minnie', last_name: "Mouse")
        ]
        @party2 = Party.create! main_event: @main_event, guests: [
          Guest.new(first_name: 'Millie', last_name: "Mouse"),
          Guest.new(first_name: 'Melody', last_name: "Mouse")
        ]
        @party3 = Party.create! main_event: @main_event, guests: [
          Guest.new(first_name: 'Pammy', last_name: "Mouse"),
          Guest.new(first_name: 'Tammy', last_name: "Mouse")
        ]
        @irrelevant_party.name.should == "The Mouses"
        @party.name.should == "The Mouses"
        @party2.name.should == "The Mouses (2)"
        @party3.name.should == "The Mouses (3)"
      end
    end
  end
  
  describe "all_invited_to?" do
    before(:each) do
      @main = MainEvent.create! short_name: 'main'
      @companion = CompanionEvent.create! short_name: 'companion', main_event: @main
      @guests = [
        Guest.new(last_name: "Zimmer"),
        Guest.new(last_name: "Gunsul"),
        Guest.new(last_name: "Frasca")
      ]
      @party = Party.create!({
        main_event: @main,
        guests: @guests
      })
    end
    it "will return false if no guests are invited to an event" do
      @party.all_invited_to?(@companion).should be_false
    end
    it "will return false if some guests are invited to an event" do
      @guests.first.invite_to(@companion)
      @party.all_invited_to?(@companion).should be_false
    end
    it "will return true if all guests are invited to an event" do
      @guests.each do |guest|
        guest.invite_to(@companion)
      end
      @party.all_invited_to?(@companion).should be_true
    end
  end
  
  describe "set_focus" do
    before(:each) do
      @joe = Guest.create! first_name: 'Joe'
      @mary = Guest.create! first_name: 'Mary'
      @party = Party.create!
      @party.guests.push(@joe, @mary)
      @party.save!
    end
    it "can set focus to a guest in this party" do
      @party.set_focus @joe
      @joe.has_focus.should be_true
    end
    it "will only have one focused guest at a time" do
      @party.set_focus @joe
      @party.set_focus @mary
      @joe.has_focus.should be_false
      @mary.has_focus.should be_true
    end
    it "will raise an error if focus is set to a non-member" do
      @oscar = Guest.create! first_name: 'Oscar'
      expect { @party.set_focus = 2 }.to raise_error
      expect { @party.set_focus @oscar }.to raise_error
    end
  end
  
  describe "attending and not_attending" do
  
    before(:each) do
      @event = MainEvent.create! short_name: "greatevent"
      @joe = Guest.create! first_name: 'Joe'
      @jimi = Guest.create! first_name: 'Jimi'
      @mary = Guest.create! first_name: 'Mary'
      @joe.invite_to(@event)
      @jimi.invite_to(@event)
      @mary.invite_to(@event)
      @joe.reply_to(@event, true)
      @jimi.reply_to(@event, true)
      @mary.reply_to(@event, false)
      @party = Party.create!
      @party.guests.push @joe, @jimi, @mary
      @party.save!
    end
  
    it "sorts guest and members into attending and not attending" do
      @party.attending(@event).should include(@joe)
      @party.not_attending(@event).should include(@mary)
    end
   
    # move to group spec?
    it "always puts the focused guest before others" do
      @party.set_focus @jimi
      @party.attending(@event).first.should == @jimi
    end
  
  end
  
end