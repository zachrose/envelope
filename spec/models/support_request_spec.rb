require 'spec_helper'
require 'support_request'

describe SupportRequest do
  before :all do
    @with_email = SupportRequest.create! email: 'x@x.com', body: 'foo'
    @without_email = SupportRequest.create! body: 'bar'
  end
  
  describe '.new' do
    it "will be" do
      @with_email.should be
      @without_email.should be
    end
  end
  
  describe 'validation' do
    it 'requires the presence of a body' do
      SupportRequest.new(body: '').valid?.should be_false
      SupportRequest.new(body: '.').valid?.should be_true
    end
  end
  
end
