require 'spec_helper'
require 'event'
require 'guest'
require 'reply_set'

describe ReplySet do
  before :all do
    @parade = MainEvent.create! short_name: 'disneyland', kind: 'parade', info: 'Parade at Cinderella Castle'
    @picnic = CompanionEvent.create! kind: 'picnic', info: 'Picnic at Big Thunder Ranch', main_event: @parade
    @mickey = Guest.new first_name: "Mickey", last_name: "Mouse"
    @minnie = Guest.new first_name: "Minnie", last_name: "Mouse"
    @party = Party.create!({
      guests: [@mickey, @minnie],
      main_event: @parade
    })
    @mickey.invite_to @picnic
    @reply_set = ReplySet.new(guest: @mickey)
  end
  describe ".new" do
    it "will be" do
      @reply_set.should be
    end
  end
  
  describe ".initialize_empty_replies" do
    before :all do
      @reply_set = @reply_set.initialize_empty_replies
    end
    it "will return self" do
      @reply_set.class.should == ReplySet
    end
    it "will initialize replies for all party_members and their events" do
      @reply_set.replies.length.should == 3
    end
    it "will have a reply for each party member and each party member's event" do
      combinations = [
        { guest: @mickey, event: @parade },
        { guest: @minnie, event: @parade },
        { guest: @mickey, event: @picnic }
      ]
      for combo in combinations
        @reply_set.replies.select { |reply| 
          reply.guest == combo[:guest] && reply.event == combo[:event]
        }.should have(1).item
      end
    end
    it "will have replies which all have a nil status" do
      @reply_set.replies.all? { |reply|
        reply.status.nil?
      }.should be_true
    end
  end
  
  describe "select_where" do
    before :all do
      @parade = Event.create kind: 'parade', info: 'Parade at Cinderella Castle'
      @picnic = Event.create kind: 'picnic', info: 'Picnic at Big Thunder Ranch'
      @mickey = Guest.create! first_name: "Mickey", last_name: "Mouse"
      @minnie = Guest.create! first_name: "Minnie", last_name: "Mouse"
      @donald = Guest.create! first_name: "Donald", last_name: "Duck"
      @minnie.update party: @mickey.party
      @donald.update party: @mickey.party
      @mickey.invite_to @parade, @picnic
      @minnie.invite_to @parade, @picnic
      @donald.invite_to @parade, @picnic
    end
    it "will return a list of replies for an event with given params" do
      @reply_set = ReplySet.new({
        guest: @mickey,
        replies: [
          Reply.new( {guest: @mickey, event: @parade, status: false}),
          Reply.new( {guest: @mickey, event: @picnic, status: false}),
          Reply.new( {guest: @minnie, event: @parade, status: false}),
          Reply.new( {guest: @minnie, event: @picnic, status: false}),
          Reply.new( {guest: @donald, event: @parade, status: false}),
          Reply.new( {guest: @donald, event: @picnic, status: false})
        ]
      })
      @reply_set.select_where({event: @parade, status: true}).length.should == 0
      @reply_set.select_where({event: @parade, guest: @mickey}).length.should == 1
      @reply_set.select_where({event: @parade}).length.should == 3
    end
  end
  
end
