require 'spec_helper'
require 'main_event'
require 'guest'

describe MainEvent do
  
  before(:each) do
    @event = MainEvent.new short_name: '4th'
  end
  
  describe "DEFAULT_KINDS" do
    it "has a class constant of default event kinds" do
      MainEvent::DEFAULT_KINDS.should == %w{party wedding dinner}
    end
  end
  
  describe "special_kind" do
    it "should return event kind if it is not a default" do
      @event.kind = 'foo'
      @event.special_kind.should == 'foo'
    end
    it "should return an empty string if it is a default" do
      @event.kind = MainEvent::DEFAULT_KINDS[0]
      @event.special_kind.should == ''
    end
  end
  
  describe "special_kind?" do
    it "should return true if kind is not a default" do
      @event.kind = 'foo'
      @event.special_kind?.should be_true
    end
    it "should return false if kind is a default" do
      @event.kind = MainEvent::DEFAULT_KINDS[0]
      @event.special_kind?.should be_false
    end
  end
  
  describe "has_companion_events?" do
    it "should return false if the event has no companion events" do
      @event.has_companion_events?.should be_false
    end
    it "should return false if the event has no saved companion events" do
      @event.companion_events.new
      @event.has_companion_events?.should be_false
    end
    it "should return true if the event has saved companion events" do
      @event.save
      @event.companion_events.create! kind: 'after-party', info: 'blah'
      @event.has_companion_events?.should be_true
    end
  end
  
  describe "self#find_guest" do
    before(:each) do
      @event = MainEvent.create! short_name: '4th'
    end
    it "find a guest with a matching first and last name under ideal conditions" do
      guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
      guest.party.main_event = @event
      guest.party.save
      @event.find_guest('Joe', 'Hendrix').should == guest
    end
    describe "returns false" do
      it "when the name is completely unfindable" do
        guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('JP', 'Morgan').should be_false
      end
      it "when search terms are blank" do
        guest = Guest.create! unknown_name: true
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('', '').should be_false
      end
      it "when matches are ambigious" do
        guests = [
          Guest.create!( first_name: 'Joe', last_name: 'Hendrix'),
          Guest.create!( first_name: 'Joe', last_name: 'Hendrix')
        ]
        guests.each do |guest|
          guest.party.main_event = @event
          guest.party.save
        end
        @event.find_guest('Joe', 'Hendrix').should be_false
      end
    end
    describe "minor phonetic spelling errors" do
      before :each do
        @expectations = [
          ['Clif','Cliff'],
          ['Sarah','Sara'],
          ['Sapperstien','Saperstein'],
          ['Max','Maxwell'],
          ['Schmidt','Smith']
        ]
        @expectations.each do |expectation|
          guest = Guest.create! first_name: expectation[0], last_name: 'Hendrix'
          guest.party.main_event = @event
          guest.party.save
        end
      end
      it "accepts names that are close enough" do
        @expectations.each do |expectation|
          correct = @event.find_guest(expectation[0], 'Hendrix')
          acceptable = @event.find_guest(expectation[1], 'Hendrix')
          correct.should == acceptable
        end
      end
      it "rejects names that are not close enough" do
        guest = Guest.create! first_name: "Mickey", last_name: 'Mouse'
        guest.party.main_event = @event
        guest.party.save
        search = @event.find_guest("Mickey", nil)
        search.should be_false
      end
    end
    describe "mononymous persons" do
      before(:each) do
        @jose = Guest.create! first_name: 'José'
        @jose.party.main_event = @event
        @jose.party.save
        @iosef = Guest.create! first_name: '', last_name: 'Iosif'
        @iosef.party.main_event = @event
        @iosef.party.save
      end
      it "is flexible with strictly mononymous persons" do
        @event.find_guest(nil, 'José').should == @jose
        @event.find_guest('Iosif', '').should == @iosef
      end
      it "is not flexible with non-mononymous persons" do
        guest = Guest.create! last_name: 'Steve', last_name: 'McQueen'
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('Steve', '').should be_false
      end
    end
    describe "whitespace insensitivity" do
      it "is whitespace-insensitive to guest input" do
        guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('Joe  ', 'Hendrix  ').should == guest
      end
      it "is whitespace-insensitive to the known name" do
        guest = Guest.create! first_name: ' Joe ', last_name: 'Hendrix  '
        guest.party.main_event = @event
        guest.party.save
        Guest.count.should == 1
        @event.find_guest('Joe', 'Hendrix').should == guest
      end
    end
    describe "case-insensitivity" do
      it "is case-insensitive to guest input" do
        guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('joe', 'hendrix').should == guest
      end
      it "is case-insensitive to the known name" do
        guest = Guest.create! first_name: 'joe', last_name: 'hendrix'
        guest.party.main_event = @event
        guest.party.save
        @event.find_guest('Joe', 'Hendrix').should == guest
      end
    end
  end
end