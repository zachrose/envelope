require 'spec_helper'
require 'guest'
require 'party'

describe Guest do
  
  describe "validation" do
    it "requires first and/or last name, unless unknown_name" do
      Guest.create( first_name: "Joe", last_name: "Hendrix" ).errors.should be_empty
      Guest.create( first_name: "Joe" ).errors.should be_empty
      Guest.create( last_name: "Hendrix" ).errors.should be_empty
      Guest.create( unknown_name: true ).errors.should be_empty
      Guest.create().errors.added? :first_name, :first_or_last_name_required
    end    
  end
  
  describe "self#find_by_name" do
    it "takes a full name and finds a guest with a matching first and last name" do
      guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
      Guest.find_by_name('Joe Hendrix').should == guest
    end
    it "takes a single name and finds a mononymous guest with a matching first name" do
      guest = Guest.create! first_name: 'Michiko'
      Guest.find_by_name('Michiko').should == guest
    end
    it "takes a single name and finds a mononymous guest with a matching last name" do
      guest = Guest.create! last_name: 'Teller'
      Guest.find_by_name('Teller').should == guest
    end
    it "takes a full name and does not find a mononymous guest with a matching first name" do
      guest = Guest.create! first_name: 'Michiko'
      Guest.find_by_name('Michiko Smith').should_not == guest
      Guest.find_by_name('Michiko Smith').should be_nil
    end
    it "takes a full name and does not find a mononymous guest with a matching last name" do
      guest = Guest.create! last_name: 'Teller'
      Guest.find_by_name('Tim Teller').should_not == guest
      Guest.find_by_name('Tim Teller').should be_nil
    end
  end
  
  describe "mononymous?" do
    it "will return false when a first and last name are present" do
      guest = Guest.create! first_name: "John", last_name: "McCracken"
      guest.mononymous?.should be_false
    end
    it "will return false when the guest has an unknown name" do
      guest = Guest.create! unknown_name: true
      guest.mononymous?.should be_false
    end
    it "will return true when first_name is blank and last_name is not blank" do
      guest = Guest.create! first_name: "Saul"
      guest.mononymous?.should be_true
    end
    it "will return true when first_name is not blank and last_name is blank" do
      guest = Guest.create! first_name: "Bruce"
      guest.mononymous?.should be_true
    end
  end
  
  describe "familiar name" do
    it "will be a first name, if available" do
      guest = Guest.create! first_name: 'Michiko'
      guest.familiar_name.should == 'Michiko'
    end
    it "will be a last name, if there is no first name" do
      guest = Guest.create! last_name: 'Teller'
      guest.familiar_name.should == 'Teller'
    end
    describe "for a guest with an unknown_name" do
      describe "where there are no replies" do
        it "will be nil" do
          guest = Guest.create! unknown_name: true
          guest.familiar_name.should be_nil
        end
      end
      describe "where there are replies" do
        it "will be the name given" do
          guest = Guest.create! unknown_name: true
          event = Event.new kind: 'party'
          reply = Reply.create! guest: guest, event: event, unknown_name: 'Shirley'
          guest.familiar_name.should == "Shirley"
        end
      end
    end
  end
  
  describe "full_name" do
    it "will be first name and last name" do
      guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
      guest.full_name.should == 'Joe Hendrix'
      guest = Guest.create! first_name: 'Michiko'
      guest.full_name.should == 'Michiko'
      guest = Guest.create! last_name: 'Teller'
      guest.full_name.should == 'Teller'
    end
    describe "for a guest with an unknown_name" do
      describe "where there are no replies" do
        it "will be nil" do
          guest = Guest.create! unknown_name: true
          guest.familiar_name.should be_nil
        end
      end
      describe "where there are replies" do
        it "will be the name given" do
          guest = Guest.create! unknown_name: true
          event = Event.new kind: 'party'
          reply = Reply.create! guest: guest, event: event, unknown_name: 'Shirley'
          guest.full_name.should == "Shirley"
        end
      end
    end
  end
  
  describe "#invite_to" do
    before(:each) do
      @guest = Guest.create! last_name: 'Teller'
      @event1 = MainEvent.create short_name: 'wedding', kind: 'wedding', info: 'some info'
      @event2 = CompanionEvent.create main_event: @event1, kind: 'rehearsal dinner', info: "some info"
      @event3 = CompanionEvent.create main_event: @event1, kind: 'after-party', info: "some info"
    end
    it "creates an invitation for a guest to an event" do
      @guest.invite_to(@event1)
      Invitation.where(guest: @guest).should have(1).items
      Invitation.where(guest: @guest, event: @event1).should have(1).items
    end
    it "creates many invitations for a guest to an array of events" do
      @guest.invite_to(@event1, @event2, @event3)
      Invitation.where(guest: @guest).should have(3).items
    end
    it "is idempotent" do
      @guest.invite_to(@event1, @event2, @event3)
      @guest.invite_to(@event1, @event2, @event3)
      Invitation.where(guest: @guest).should have(3).items
    end
  end
  
  describe "#invited_to?" do
    before :each do
      @guest = Guest.create! last_name: 'Teller'
      @event1 = MainEvent.create short_name: 'wedding', kind: 'wedding', info: 'some info'
      @event2 = CompanionEvent.create main_event: @event1, kind: 'rehearsal dinner', info: "some info"
      @event3 = CompanionEvent.create main_event: @event1, kind: 'after-party', info: "some info"
    end
    it "will be false if the guest is not invited to the event" do
      @guest.invited_to?(@event1).should be_false
    end
    it "will return true if the guest is invited to the event" do
      @guest.invite_to(@event1)
      @guest.invited_to?(@event1).should be_true
    end
  end
  
  describe "#events_invited" do
    before :each do
      @main_event = MainEvent.create! short_name: 'mainevent'
      @companion1 = @main_event.companion_events.create! kind: 'bat mizvah'
      @companion2 = @main_event.companion_events.create! kind: 'bar mizvah'
      @events = [@main_event, @companion1, @companion2]
      @guest = Guest.create! last_name: "Teller"
      @guest.party.main_event = @main_event
      @guest.party.save!
    end
    it "will include the guest's party's main event" do
      @guest.events_invited.should include(@main_event)
    end
    
    it "will not include companion events that the guest is not invited to" do
      @guest.events_invited.should_not include @companion1
      @guest.events_invited.should_not include @companion2
    end
    it "will include all the events the guest is invited to" do
      @guest.invite_to @main_event, @companion1
      @guest.events_invited.should include @main_event, @companion1
    end
  end
  
  describe "#reply_to" do
    before :each do
      @main_event = MainEvent.create! short_name: 'mainevent'
      @companion1 = @main_event.companion_events.create! kind: 'bat mizvah'
      @companion2 = @main_event.companion_events.create! kind: 'bar mizvah'
      @events = [@main_event, @companion1, @companion2]
      @guest = Guest.create! last_name: "Teller"
      @guest.invite_to @main_event, @companion1
    end
    it "should create a reply with truthy status when replying with a positive" do
      @guest.reply_to @main_event, true
      Reply.where(guest: @guest, event: @main_event, status: true).should have(1).items
    end
    it "should create a reply with falsey status when replying with a negative" do
      @guest.reply_to @main_event, false
      Reply.where(guest: @guest, event: @main_event, status: false).should have(1).items
    end
    it "is NOT idempotent" do
      @guest.reply_to @main_event, true
      @guest.reply_to @main_event, false
      Reply.where(guest: @guest, event: @main_event).should have(2).items
      Reply.where(guest: @guest, event: @main_event, status: true).should have(1).items
      Reply.where(guest: @guest, event: @main_event, status: false).should have(1).items
    end
  end
  
  describe "#replied_to?" do
    before :each do
      @main_event = MainEvent.create! short_name: 'mainevent'
      @companion1 = @main_event.companion_events.create! kind: 'bat mizvah'
      @companion2 = @main_event.companion_events.create! kind: 'bar mizvah'
      @events = [@main_event, @companion1, @companion2]
      @guest = Guest.create! last_name: "Teller"
      @guest.invite_to @main_event, @companion1
    end
    it "should return true when a reply has been made" do
      @guest.reply_to @main_event, true
      @guest.replied_to?(@main_event).should be_true
      @guest.replied_to?(@companion1).should_not be_true
    end
    it "should return false when a reply has not been made" do
      @guest.reply_to @main_event, false
      @guest.replied_to?(@main_event).should be_true
      @guest.replied_to?(@companion1).should be_false
    end
  end
  
  describe "#events_attending" do
    before :each do
      @main_event = MainEvent.create! short_name: 'mainevent'
      @companion1 = @main_event.companion_events.create! kind: 'bat mizvah'
      @companion2 = @main_event.companion_events.create! kind: 'bar mizvah'
      @events = [@main_event, @companion1, @companion2]
      @guest = Guest.create! last_name: "Teller"
      @guest.party.main_event = @main_event
      @guest.party.save!
      @guest.invite_to @main_event, @companion1
    end
    it "will not include events the guest has not replied to" do
      @guest.events_attending.should be_empty
    end
    it "will not include events the guest has replied to as not attending" do
      @guest.reply_to @main_event, false
      @guest.events_attending.should_not include @main_event
    end
    it "will include events the guest has replied to as attending" do
      @guest.reply_to @main_event, true
      @guest.reply_to @companion1, true
      @guest.events_attending.should include @main_event, @companion1
    end
    it "will respect only the latest reply from a guest" do
      @guest.reply_to @main_event, true
      @guest.events_attending.should include @main_event
      @guest.reply_to @main_event, false
      @guest.events_attending.should_not include @main_event
    end
  end
  
  describe "#events_not_attending" do
    before :each do
      @main_event = MainEvent.create! short_name: 'mainevent'
      @companion1 = @main_event.companion_events.create! kind: 'bat mizvah'
      @companion2 = @main_event.companion_events.create! kind: 'bar mizvah'
      @events = [@main_event, @companion1, @companion2]
      @guest = Guest.create! last_name: "Teller"
      @guest.party.main_event = @main_event
      @guest.party.save!
      @guest.invite_to @main_event, @companion1
    end
    it "will not include events the guest has not replied to" do
      @guest.events_not_attending.should be_empty
    end
    it "will not include events the guest has replied to as attending" do
      @guest.reply_to @main_event, true
      @guest.events_not_attending.should_not include @main_event
    end
    it "will include events the guest has replied to as not attending" do
      @guest.reply_to @main_event, false
      @guest.reply_to @companion1, false
      @guest.events_not_attending.should include @main_event, @companion1
    end
    it "will respect only the latest reply from a guest" do
      @guest.reply_to @main_event, false
      @guest.events_not_attending.should include @main_event
      @guest.reply_to @main_event, true
      @guest.events_not_attending.should_not include @main_event
    end
  end
  
  describe "#status_for" do
    before :each do
      @event = MainEvent.create! short_name: 'mainevent'
      @companion = @event.companion_events.create! kind: 'bat mizvah'
      @guest = Guest.create! last_name: "Teller"
    end
    it "will return not_invited when no invitation exists for that guest and event" do
      @guest.status_for(@companion).should == :not_invited
    end
    it "will return invited when guest's party belongs to the event" do
      @guest.party.update(main_event: @event)
      @guest.status_for(@event).should == :invited
    end
    it "will return invited when there is an invitation but not a reply" do
      @guest.invite_to @companion
      @guest.status_for(@companion).should == :invited
    end
    it "will return confirmed when a positive reply exists" do
      @guest.invite_to @companion
      @guest.reply_to @companion, true
      @guest.status_for(@companion).should == :confirmed
    end
    it "will return declined when a negative reply exists" do
      @guest.invite_to @companion
      @guest.reply_to @companion, false
      @guest.status_for(@companion).should == :declined
    end
  end
  
  describe "focus" do
    it "can be set" do
      guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
      guest.focus
      guest.has_focus.should be_true
    end
    it "can be unset" do
      guest = Guest.create! first_name: 'Joe', last_name: 'Hendrix'
      guest.focus
      guest.unfocus
      guest.has_focus.should be_false
    end
  end
  
  describe "party" do
    it "can be specified on create" do
      party = Party.create! name: "Singleman"
      guest = Guest.create! first_name: "Joe", party: party
      guest.party.should == party
    end
    it "will be created if not specified" do
      guest = Guest.create! first_name: "Joe"
      guest.party.should be
    end
  end
  
  describe "party_members" do
    it "is a list of all the other people in the guest's party" do
      party = Party.create! name: "Party"
      guest = Guest.create! first_name: "Guest", party: party
      fellow1 = Guest.create! first_name: "Fellow 1", party: party
      fellow2 = Guest.create! first_name: "Fellow 2", party: party
      guest.party_members.length.should == 2
      guest.party_members.should include(fellow1, fellow1)
    end
  end
  
end



