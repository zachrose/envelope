require 'spec_helper'
require 'admin'

describe Admin do
  describe "validation" do
    it "needs an email, password, and short_name for a first event" do
      admin = Admin.create! email: "x@x.com", password: "12345678", first_event_short_name: "xx"
    end
    it "must have a unique email" do
      admin1 = Admin.create email: "x@x.com", password: "12345678", first_event_short_name: "xx"
      admin2 = Admin.new email: "x@x.com", password: "12345678", first_event_short_name: "xx"
      admin2.valid?.should be_false
    end
    describe "short_name" do
      it "must have be case-insensitive unique" do
        admin1 = Admin.create email: "x@x.com", password: "12345678", first_event_short_name: "xx"
        admin2 = Admin.create email: "x@y.com", password: "12345678", first_event_short_name: "xx"
        admin3 = Admin.create email: "x@y.com", password: "12345678", first_event_short_name: "XX"
        admin2.valid?.should be_false
        admin2.errors[:events].should be
        admin3.valid?.should be_false
        admin3.errors[:events].should be
      end
      it "must not be in the list of reserved names" do
        admin = Admin.create email: "x@x.com", password: "12345678", first_event_short_name: "help"
        admin.valid?.should be_false
        admin.errors[:events].should be
      end
      it "must not have any spaces or unapproved punctation" do
        good_names = ["this-is-fine","3r2","fine33"]
        good_names.each_with_index do |name, i|
          admin = Admin.create(first_event_short_name: name, email: "x#{i}@x.com", password: "12345678")
          admin.valid?.should be_true
        end
        bad_names = [ "oh snap", "oh_snap", "uhoh!", "uh.oh", "uhoh::fff" "ert%%qq", "oh<asd>"]
        bad_names.each_with_index do |name, i|
          admin = Admin.create(first_event_short_name: name, email: "x#{i}@x.com", password: "12345678")
          admin.valid?.should be_false
          admin.errors[:events].should be
        end
      end
    end
  end
  describe "after create" do
    it "has at least one event" do
      admin = Admin.create! email: "x@x.com", password: "12345678", first_event_short_name: "xx"
      admin.main_events.should have(1).items
      admin.main_events.first.kind.should == MainEvent::DEFAULT_KINDS[0]
    end
  end
end