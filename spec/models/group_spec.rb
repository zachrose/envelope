require 'spec_helper'
require 'group'
require 'guest'

describe Group do
  
  before do
    thing = double()
    thing.stub has_focus: true
    @things = ["thing1", thing]
    @group = Group.new @things, 3
  end
  
  it "should create a new instance given valid attributes" do
    Group.new @things, 3
  end
  
  it "should know if it has all the guests" do
    @group.all?.should be_false
  end

  it 'should know if it has two guests' do
    @group = Group.new [:a, :b], 2
    @group.two?.should be_true
  end
  
  it 'should know if it has more than two guests' do
    @group = Group.new [:a, :b, :c], 3
    @group.more_than_two?.should be_true
  end

  it "should know if it has many guests" do
    @group.many?.should be_true
  end

end