namespace :prerender do

  desc "Renders error pages and sticks them where they belong"
  task errors: :environment do
    av = ActionView::Base.new(Rails.root.join('app','views'))
    ['404','500'].each do |status|
      av.assign(status:status)
      File.open(Rails.root.join('public',"#{status}.html"), 'w') do |f|
        f.write av.render layout: 'layouts/errors', template: 'homepage/error'
      end
    end
  end

end
