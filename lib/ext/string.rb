require 'active_support'
require 'inflector_extensions'

class String
  
  def nice_title
    self.split('-').map{|w|w.titleize}.join('-')
  end  
  
  def pluralize_regular
    if self.ends_with?('s','x','z','ch','sh')
      self+'es'
    else
      self+'s'
    end
  end
  
end