require 'event'
require 'rubyfish'

class MainEvent < Event
  
  DEFAULT_KINDS = %w(party wedding dinner)
  RESERVED_SITE_NAMES = %w(
    help support pricing services testimonials reviews subscribe
    inquiry payments login logout dashboard about privacy terms
    policies policy copyright licensing links media information
    store blog photos guest guests contact info email news
    newsletter magazine guarantee survey events calendar search
    return refund returns refunds images faqs faq admin tour
    example intro introduction envelope errors www
  )
  
  JARO_WINKLER_THRESHOLD = 0.86
  
  belongs_to :admin
  has_many :companion_events
  has_many :parties
  has_many :unexpected_guest_messages
  accepts_nested_attributes_for :companion_events
  
  validates_presence_of :short_name
  validates :short_name, uniqueness: { case_sensitive: false }
  validates :short_name, exclusion: { 
    in: RESERVED_SITE_NAMES
  }
  validates_format_of(
    :short_name, {
      without: /[^a-zA-Z\d-]/,
      message: :spaces_and_punctuation
    }
  )
  
  def find_guest(first_name, last_name)
    if first_name.blank? && last_name.blank?
      return false
    end
    found_guests = find_guests_by_near_exact_match(first_name, last_name)
    if found_guests.none?
      found_guests = find_guests_phonetically(first_name, last_name)
    end
    if found_guests.none?
      found_guests = find_mononymous_guests(first_name, last_name)
    end
    if found_guests.none?
      found_guests = find_guests_by_string_similarity(first_name, last_name)
    end
    if found_guests.one?
      return found_guests.first
    else
      return false
    end
  end
  
  private
  
  def find_mononymous_guests(first_name, last_name)
    name = (first_name.blank? ? last_name : first_name).downcase
    guests
      .select(&:mononymous?)
      .select do |guest|
        first = guest.first_name.to_s
        last = guest.last_name.to_s
        (name == first.downcase) || (name == last.downcase)
      end
  end
  
  def find_guests_by_string_similarity(first_name, last_name)
    guest = guests.select do |guest|
      first_distance = RubyFish::JaroWinkler.distance(first_name, guest.first_name)
      last_distance = RubyFish::JaroWinkler.distance(last_name, guest.last_name)
      first_distance > JARO_WINKLER_THRESHOLD && last_distance > JARO_WINKLER_THRESHOLD
    end.max do |guest|
      one = [first_name, last_name].join(' ')
      two = guest.full_name
      distance = RubyFish::JaroWinkler.distance(one,two)
    end
    [guest]
  end
  
  def find_guests_by_near_exact_match(first_name, last_name)
    guests.select do |guest|
      known_first = guest.first_name.to_s.downcase.strip
      known_last = guest.last_name.to_s.downcase.strip
      trying_first = first_name.to_s.downcase.strip
      trying_last = first_name.to_s.downcase.strip
      (known_first == trying_first) && (known_last == trying_last)
    end
  end
  
  def find_guests_phonetically(first_name, last_name)
    guests.select do |guest|
      one = RubyFish::DoubleMetaphone.phonetic_code([first_name, last_name].join(' '))
      two = RubyFish::DoubleMetaphone.phonetic_code(guest.full_name)
      intersection = one & two
      intersection.any?
    end
  end
  
  def guests
    self.parties.map(&:guests).flatten.reject(&:unknown_and_blank_name)
  end
  
end