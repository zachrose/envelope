class Event < ActiveRecord::Base
  
  attr_accessor :other_kind
  has_many :replies # needed?
  
  def special_kind
    self.class::DEFAULT_KINDS.include?(self.kind) ? '' : self.kind
  end
  
  def special_kind?
    self.class::DEFAULT_KINDS.include?(self.kind) ? false : true
  end
  
  def to_param
    short_name
  end
  
  def has_companion_events?
    self.companion_events.reject{ |companion_event|
      companion_event.new_record?
    }.any?
  end
  
end