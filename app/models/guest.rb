require 'invitation'
require 'reply'

class Guest < ActiveRecord::Base

  validates_presence_of :first_name,
    message: :first_or_last_name_required,
    unless: [
      :last_name?,
      :unknown_name?
    ]
  
  belongs_to :party
  has_one :main_event, through: :party
  has_many :invitations
  has_many :replies
  
  attr_accessor :has_focus
  
  after_create :create_party
  
  def self.find_by_name(name)
    query = <<-SQL
      first_name || \' \' || last_name = (?) OR
      first_name = (?) OR
      last_name = (?)
    SQL
    search = self.where(query, name, name, name)
    return search.first
  end
  
  def mononymous?
    return false if unknown_name
    last = first_name.blank? and !last_name.blank?
    first = last_name.blank? and !first_name.blank?
    first || last
  end
  
  def familiar_name
    if unknown_name
      Reply.where(guest: self).map(&:unknown_name).compact.last
    else
      first_name || last_name
    end
  end
  
  def unknown_and_blank_name
    unknown_name and first_name.blank? and last_name.blank?
  end
  
  def full_name
    if unknown_name
      return Reply.where(guest: self).map(&:unknown_name).compact.last
    end
    names = [first_name, last_name].compact
    if names.any? then names.join ' ' end
  end
  
  def party_members
    party.guests.reject { |g| g == self }
  end
  
  def invite_to(*events)
    events.each do |event|
      Invitation.where(guest: self, event: event).first_or_create
    end
  end
  
  def invited_to?(event)
    one = Invitation.where(guest: self, event: event).any?
    two = self.party.main_event == event
    one || two
  end
  
  def reply_to(event, status)  
    reply = Reply.create(guest: self, event: event, status: status)
  end
  
  def replied_to?(event)  
    Reply.where(guest: self, event: event).any?
  end
  
  def events_invited
    main = self.party.main_event
    unless main.companion_events.any?
      return [main]
    end
    ces = main.companion_events.select do |event|
      self.invited_to?(event)
    end
    [main].concat ces
  end
  
  def events_attending
    events = self.events_invited
    events.select do |event|
      reply = Reply.where(guest: self, event: event).last(1)
      reply[0] && reply[0].status == true
    end
  end
  
  def events_not_attending
    events = self.events_invited
    events.select do |event|
      reply = Reply.where(guest: self, event: event).last(1)
      reply[0] && reply[0].status == false
    end
  end
  
  def status_for(event)
    
    last_reply = Reply.where(event: event, guest: self).last
    confirmed = last_reply ? (last_reply.status == true) : nil
    declined = last_reply ? (last_reply.status == false) : nil
    invited = self.invited_to?(event)
    
    if confirmed
      return :confirmed
    elsif declined
      return :declined
    elsif invited
      return :invited
    else
      return :not_invited
    end
  end
  
  def focus
    self.has_focus = true
  end
  
  def unfocus
    self.has_focus = false
  end
  
  def <=>(other)
    self.has_focus ? 0 : 1
  end
  
  private
  
  def create_party
    unless self.party
      self.party = Party.create!
      self.save!
    end
  end
  
end
