class Group < Array
  def initialize folks, party_length
    self.party_length = party_length
    super(folks)
    self.sort!
  end

  attr_accessor :party_length

  def all?
    length == party_length
  end

  def two?
    length == 2
  end
  
  def more_than_two?
    length > 2
  end

  def many?
    length > 1
  end
  
end