class Reply < ActiveRecord::Base
  
  belongs_to :guest
  belongs_to :event
  belongs_to :reply_set
  
  validates_presence_of :guest
  validates_presence_of :event
  
  # TODO: why this fail?
  validates_inclusion_of :status, in: [true, false, nil]
  
  
end