require 'event'

class CompanionEvent < Event
  
  belongs_to :main_event
  
  DEFAULT_KINDS = ['after-party', 'rehearsal dinner']
  
end
