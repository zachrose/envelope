require 'devise'
require 'main_event'

class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_many :main_events
  validates_associated :main_events
  
  validates_presence_of :first_event_short_name
  validates :first_event_short_name, exclusion: { 
    in: MainEvent::RESERVED_SITE_NAMES
  }
  validates_format_of(
    :first_event_short_name, {
      without: /[^a-zA-Z\d-]/,
      message: :spaces_and_punctuation
    }
  )
  
  attr_accessor :first_event_short_name
  
  before_save :ensure_main_event
  
  private
  
  def ensure_main_event
    if self.main_events.empty?
      self.main_events.build short_name: self.first_event_short_name, kind: MainEvent::DEFAULT_KINDS[0]
    end
  end
  
end
