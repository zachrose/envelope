require 'group'

class Party < ActiveRecord::Base
  
  has_many :guests, :dependent => :destroy
  accepts_nested_attributes_for :guests
  belongs_to :main_event
  
  before_create :name_after
  
  class GuestNotInParty < StandardError; end
  
  def name_after
    if !self.name # ?
      unique_formal_names = formal_names.uniq
      if (!unique_formal_names || unique_formal_names.length < 1)
        self.name = self.guests.map(&:first_name).to_sentence
      elsif unique_formal_names.length == 1
        if formal_names.length == 1
          self.name = unique_formal_names[0]
        else
          self.name = 'The '+unique_formal_names[0].pluralize_regular
        end
      elsif unique_formal_names.length > 1
        self.name = unique_formal_names.to_sentence
      end
      increment_name
    end
  end
  
  def set_focus(guest)
    raise GuestNotInParty unless includes_guest?(guest)
    guests.each { |guest| guest.unfocus }
    guest.focus
  end
  
  def attending(event)
    Group.new(guests.select do |guest|
      reply = Reply.where(guest: guest, event: event).last(1)
      reply[0] && reply[0].status == true
    end, guests.length)
  end

  def all_invited_to?(event)
    guests.all? { |guest| guest.invited_to?(event) }
  end

  def not_attending(event)
    Group.new(guests.select do |guest|
      reply = Reply.where(guest: guest, event: event).last(1)
      reply[0] && reply[0].status == false
    end, guests.length)
  end
  
  private
  
  def formal_names
    guests.map(&:last_name).reject(&:blank?)
  end
  
  def includes_guest?(guest)
    self.guests.include? Guest.find(guest.id)
  end

  # TODO: Make this atomic somehow?
  def increment_name
    
    
    if self.main_event_id
      count = Party.where("main_event_id = ? AND name like ?", self.main_event_id, "#{self.name}%").count
    else
      # TODO: This is fucked.
      # Not all unit tests include a main_event.
      # All unit tests should be rewritten to include a main_event?
      count = Party.where("name like ?", "#{self.name}%").count
    end
    
    if count >= 1
      self.name = "#{self.name} (#{count+1})"
    end
  end
  
end
