class ReplySet < ActiveRecord::Base
  belongs_to :guest
  has_many :replies
  accepts_nested_attributes_for :replies
    
  def initialize_empty_replies
    party = self.guest.party
    self.replies = []
    for guest in party.guests
      for event in guest.events_invited
        unless event.type == "CompanionEvent" and guest.unknown_name
          self.replies.push Reply.new guest: guest, event: event
        end
      end
    end
    return self
  end
  
  def blank_unknown_names_to_nil
    replies.each do |reply|
      if reply.unknown_name.blank?
        reply.unknown_name = nil
      end
    end
  end
  
  def select_where(hash)
    self.replies.select do |reply|
      hash.keys.all? do |key|
        reply.send(key) == hash[key]
      end
    end
  end
  
  def make_assumptions
    known_guests = replies.select {|reply| !reply.guest.unknown_name }.map(&:guest)
    unknown_guests = replies.select {|reply| reply.guest.unknown_name }.map(&:guest)
    
    events_attended_by_known_guests = replies.map { |reply|
      if reply.guest.unknown_name.nil? && (reply.status == true)
        reply.event
      end
    }.compact
    
    unknown_guest_not_attending_something = replies.any?{ |reply|
      reply.guest.unknown_name && (reply.status == false)
    }
    
    unless unknown_guest_not_attending_something
      events_attended_by_known_guests.each do |event|
        unknown_guests.each do |guest|
          self.replies.new( guest: guest, event: event, status: true)
        end
      end
    end
    
  end
  
end
