class MessagesController < ApplicationController
  
  before_filter :authenticate_admin!, only: [:index]
  before_action :set_event, only: [:index]
  layout 'dashboard', only: [:index]
  
  def index
    
  end
  
  def create
    @message = UnexpectedGuestMessage.new(message_params)
    if @message.save
      render action: 'thanks', status: :created
    else
      render action: 'new'
    end
  end
  
  private
  
  def message_params
    params.require(:unexpected_guest_message)
      .permit(
        :main_event_id,
        :body,
        :first_name,
        :last_name)
  end
  
  def set_event
    @event = MainEvent.find_by_short_name params.require :short_name
  end
  
end