class ReplySetsController < ApplicationController
  
  before_action :discourage_robots
  
  def hello
    event
  end
  
  def new
    @guest = event.find_guest(*guest_name_params)
    if @guest
      @reply_set = ReplySet.new(guest: @guest).initialize_empty_replies
      render :new
    else
      @message = UnexpectedGuestMessage.new(main_event: event, first_name: guest_name_params[0], last_name: guest_name_params[1])
      render 'messages/new'
    end
  end
  
  def create
    guest_id = request.params[:reply_set][:guest_id]
    @guest = Guest.find(guest_id)
    @reply_set = ReplySet.new(reply_set_params)
    @reply_set.blank_unknown_names_to_nil
    @reply_set.make_assumptions

      if @reply_set.save
        @reply_set = ReplySetPresenter.new @reply_set
        render :create
      else
        flash.alert = t('something_wrong')
        render action: 'new'
      end

  end
  
  private
  
  def event
    MainEvent.find_by_short_name(params.require(:event)) || event_site_not_found
  end
  
  def guest_name_params
    [
      params[:first_name],
      params[:last_name]
    ]
  end
    
  
  def discourage_robots
    response.headers["X-Robots-Tag"] = "noindex, nofollow"
  end

  def event_site_not_found
    raise ActionController::RoutingError.new('No such event site')
  end
  
  def reply_set_params
    
    params[:reply_set][:replies_attributes].each do |_, replies_attributes|
      if replies_attributes[:status] == "true" then replies_attributes[:status] = true end
      if replies_attributes[:status] == "false" then replies_attributes[:status] = false end
    end
    
    params.require(:reply_set).permit(
      :guest_id,
      replies_attributes: [
        :guest_id,
        :event_id,
        :status,
        :unknown_name
      ]
    )
  end
  
end
