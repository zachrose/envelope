class GuestsController < ApplicationController
  
  before_filter :authenticate_admin!
  
  before_action :set_event, only: [:index]
  
  layout 'dashboard'
  
  # No apparent way to add new child records in a form.
  # Initializing 'a big number', and displaying as needed.
  MAX_GUESTS_IN_PARTY = 10
  
  def index
  end

  private
  
  def set_event
    @event = current_admin.main_events.find_by_short_name(params[:short_name])
  end

end
