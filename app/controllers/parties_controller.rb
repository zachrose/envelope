class PartiesController < ApplicationController
  
  before_filter :authenticate_admin!
  
  before_action :set_event, only: [:new, :new_guest, :create]
  before_action :set_party, only: [:edit, :update]
  
  layout 'dashboard'
  
  # No apparent way to add new child records in a form.
  # Initializing 'a big number', and displaying as needed.
  MAX_NEW_GUESTS_IN_PARTY = 10
  
  def new
    @party = Party.new main_event: @event
    add_blank_guests
  end
  
  def new_guest
    @party = Party.new main_event: @event
    add_blank_guest
  end
  
  def edit
    add_blank_guests
  end
  
  def create
    @party = Party.new(party_params)
    
    if additional_guest_param
      @party.guests.new unknown_name: true
    end
    
    begin
      Party.transaction do
        @party.save!
        initialize_invitations.each &:save!
      end
    rescue
      # TODO: handle this
      render action: 'new'
    else
      redirect_to guests_show_path(@event), :anchor => @party.id
    end
  end
  
  def update
    @party.assign_attributes(party_params)
    begin
      Party.transaction do
        @party.save!
        initialize_invitations.each &:save!
      end
    rescue
      # TODO: handle this
      render action: 'new'
    else
      redirect_to guests_show_path(@event), :anchor => @party.id
    end
  end
    
  private

  def add_blank_guest
    @party.guests.new
  end
  
  def add_blank_guests
    MAX_NEW_GUESTS_IN_PARTY.times do
      @party.guests.new
    end
  end

  def initialize_invitations
    invitations = []
    @party.guests.each do |guest|
      invitations.push Invitation.new guest_id: guest.id, event_id: @event
    end
    @party.guests.each.with_index do | guest, index |
       unless (invitations_params.nil? || invitations_params['event'].nil? )
         event_id = invitations_params['event'].keys[0]
         invited = invitations_params['event'][event_id] == 'yes'
         if invited
           invitations.push Invitation.new guest_id: guest.id, event_id: event_id
         end
       end
     end
    invitations
  end

  def set_event
    @event = current_admin.main_events.find_by_short_name(params[:short_name])
  end
  
  def set_party
    set_event
    @party = @event.parties.find(params[:id])
  end

  def party_params
    params.require(:party).permit(
      :main_event_id,
      guests_attributes: [
        :id,
        :first_name,
        :last_name
      ]
    )
  end
  
  def additional_guest_param
    params.has_key?(:additional_guest) && params.require(:additional_guest) == 'yes'
  end
  
  def invitations_params
    params.require(:invitations).permit!
  end

end
