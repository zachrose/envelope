class EventsController < ApplicationController
  
  before_filter :authenticate_admin!
  
  before_action :set_event, only: [:show, :update]
  
  layout 'dashboard'
  
  def index
    @events = current_admin.main_events.all
  end
  
  def show
    @event.companion_events.new main_event: @event, kind: CompanionEvent::DEFAULT_KINDS[0]
  end
  
  def update
    respond_to do |format|
      if @event.update(event_params)
        flash[:notice] = 'Event updated.'
        format.html { redirect_to @event } #, notice: 'Event updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'show' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
    
  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end
    
  private

  def set_event
    @event = current_admin.main_events.find_by_short_name(params[:short_name])
  end

  def event_params
    if params[:main_event][:other_kind] && params[:main_event][:kind] == '__custom__'
      params[:main_event][:kind] = params[:main_event][:other_kind]
    end
    
    if params[:main_event][:companion_events_attributes]
      params[:main_event][:companion_events_attributes].each do |_, companion_event_attributes|
        if companion_event_attributes[:other_kind] && companion_event_attributes[:kind] == '__custom__'
          companion_event_attributes[:kind] = companion_event_attributes[:other_kind]
        end
      end
    end
    
    params.require(:main_event).permit(
      :info,
      :kind,
      companion_events_attributes: [
        :id,
        :info,
        :kind
      ]
    )
  end

end