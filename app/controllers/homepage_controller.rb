class HomepageController < ApplicationController
  
  def index
  end
  
  def about
  end
  
  def privacy
    @long_page = true
  end
  
  def error
    @status = params.require 'status'
    render layout: 'errors'
  end
  
  def tour
    if params['page'] == 'last'
      @next = new_admin_registration_path
      render template: "tour/last"
      return true
    end
    page = (params['page'] || 1).to_i
    if (page < 1) then page = 1 end
    next_page = page == 12 ? 'last' : page + 1 # avoid bad luck
    @next = tour_path page: next_page
    render template: "tour/#{page}"
  end
  
  private
  
  def tour_pages_count
    Envelope::Application.config.tour_pages_count
  end
  
end
