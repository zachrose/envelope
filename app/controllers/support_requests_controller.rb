class SupportRequestsController < ApplicationController
  
  def new
    email = current_admin && current_admin.email ? current_admin.email : nil
    @support_request = SupportRequest.new email: email
  end
  
  def create
    @support_request = SupportRequest.new(support_request_params)
    respond_to do |format|
      if @support_request.save
        @notice = true
        format.html { render action: 'new', status: :created }
        format.json { render action: 'new', status: :created }
      else
        format.html { render action: 'new' }
        format.json { render json: @support_request.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
  
  def support_request_params
    params.require(:support_request).permit(:email, :body)
  end
  
end