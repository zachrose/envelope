class RepliesController < ApplicationController

  before_filter :authenticate_admin!

  def create
    @reply = Reply.new(reply_params)
    respond_to do |format|
      if @reply.save
        status = @reply.guest.status_for(@reply.event)
        # format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render json: {status: t(status)}, status: :created }
      else
        # format.html { render action: 'new' }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reply_params
      params.require(:reply).permit(:event_id, :guest_id, :status)
    end
end
