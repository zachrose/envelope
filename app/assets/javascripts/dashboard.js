//= require jquery
//= require jquery_ujs
//= require jquery
//= require underscore
//= require redactor
//= require input_aside
//= require input_touched

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-47470989-1', 'velo.pe');
ga('send', 'pageview');

$(function(){
  
  // show and enable hidden companion event form
  $('.add_companion_event').click(function(){
    $(this).closest('fieldset').siblings('.companion_event.new').show().removeAttr('disabled')
    $(this).parents('form').addClass('with_companions');
    $(this).parent().hide();
  });
  
  // hide and disable new companion event form
  $('.companion_event.new legend .remove.link').click(function(){
    $(this).closest('fieldset').attr('disabled', 'disabled').hide()
    $(this).parents('form').removeClass('with_companions');
    $(this).parents('form').find('#main_event .does_event_have_companion label').show()
  });
  
  $('.companion_event:not(.new) legend .remove.link').click(function(){
    var $this = $(this);
    var event_id = $this.parents('fieldset').data('id')
    if(!confirm('Are you sure you want to delete this companion event?')){
      return false;
    }
    if(!event_id){
      alert('Sorry, something went wrong');
      return false;
    }
    $.ajax({
      url: '/events/'+event_id,
      type: 'DELETE',
      dataType: 'json',
      processData: false,
      success: function(reply){          
        $this.parents('fieldset').remove();
      },
      error: function(){
        alert('Sorry, something went wrong');
      }
    });
  });
  
  // hide and disable new companion event form
  $('.companion_event.new legend .remove.link').click(function(){
    var fieldset = $(this).closest('fieldset');
    fieldset.attr('disabled', 'disabled')
    fieldset.hide()
    var form = $(this).parents('form');
    form.removeClass('with_companions');
  });
  
  // customize redactor
  $('.redactor_content').redactor({
    
    tabFocus: false,
    minHeight: 100,
    buttons: [
      'html', '|', 'formatting', '|',
      'bold', 'italic', '|',
      'unorderedlist', 'orderedlist', '|',
      'image', 'file', 'link']
  });
  
  // autofocus on .other text field
  $('.field.kind select').change(function(){
    if($(this).val() == '__custom__'){
      $(this).parent('.field').attr('class','field kind custom');
      $(this).siblings('input.other').focus();
    }
    else{
      $(this).parent('.field').attr('class','field kind default');
    }
  });
  
  // enable surrounding hints
  $('input.other').focus(function(){
    $(this).parent('.field').addClass('explain');
  });
  
  Envelope.input_aside('input.other');
  
  $('.invite_to label').click(function(){
    $(this).siblings().filter(function(){return this.value=='yes'}).click()
  })
  $('label[for=additional_guest]').click(function(){
    var cb = $(this).siblings('input').filter(function(i, sibling){
      return sibling.type == 'checkbox'
    });
    cb[0].checked = !cb[0].checked;
  })
  
  $('.add_another_guest').click(function(){
    var link = this;
    var fieldset = $(this).parents('form').find('fieldset:disabled').first()
    enable(fieldset);
    function enable(el){
      el.removeAttr('disabled');
      el.find('*:disabled').removeAttr('disabled');
      el.find('input:first').focus();
      has_next = $(link).parents('form').find('fieldset:disabled').length
      if(!has_next){ $(link).hide() } else { $(link).show() }
    }
  })
  
  $('.guest .status .edit').click(function(event){
    // hide others
    $('.show_options').removeClass('show_options');
    // prepare to close
    event.stopPropagation();
    $(document).one('click', function(){
      $('.show_options').removeClass('show_options');
    });
    // await action
    var $edit = $(this)
    $edit.addClass('show_options');
    var current_status = $(this).parent('.status').data('status');
    var guest_id = $(this).parents('.guest').data('guest-id');
    var event_id = $(this).parent('.status').data('event-id');
    $(this).find('button').click(function(){
      var status, desired_status = $(this).attr('class');
      if(desired_status === 'confirmed'){ status = true; }
      if(desired_status === 'declined'){ status = false; }
      if(desired_status === 'unknown'){ status = null; }
      var data = { guest_id: guest_id, event_id: event_id, status: status }
      var $status = $(this).parents('.status').find('span.status')
      $.ajax({
        url: '/replies',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: function(reply){          
          $status.text(reply.status);
          $edit.removeClass('show_options');
        },
        error: function(){
          alert('Sorry, something went wrong');
        }
      })
    })
  });
  
  $('.guest .status .edit').click(function(){
    $(this).addClass('show_options')
  });
  
});
  