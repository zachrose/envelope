var Envelope = window.Envelope || {}

Envelope.input_aside = function(selector){
  $(selector).bind('focus keyup', function(){
    var this_value = $(this).val() || '___';
    var field = $(this).parents('.field')
    var aside = $(field).find('aside');
    var spans = $(aside).find('span');
    if (!(spans.length)){
      var newHTML = $(aside).html().split('___').join('<span></span>');
      $(aside).html(newHTML);
    }
    var span = $(aside).find('span');
    span.text(this_value);
  });
}