class window.Router
  constructor: (options)->
    options = options || {}
    @default = options.default
  start: ->
    $(document).ready @update
    window.addEventListener 'hashchange', @update
  update: =>
    document.body.className = @hash() || @default
  hash: ->
    _.last document.location.hash.split '#'