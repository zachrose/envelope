//= require input_aside

$(function(){
  $('.unknown_name .answer input[value=true]').click(function(){
    $(this).parents('.unknown_name').find('.field.name').show();
  });
  
  // provide live feedback to surrounding aside
  Envelope.input_aside('input#admin_first_event_short_name');
  
  // mixpanel
  if(!!$('h1:contains(Kindreply)').length){
    mixpanel.track('homepage');
  }
  var links = [
    'login',
    'create_a_site',
    'try_it_link',
    'about',
    'privacy',
    'help'
  ];
  for(var i = 0, l = links.length; i < l; i++){
    var link = links[i];
    $('a.'+link).click(function(){mixpanel.track(link)});
  }
  
});
