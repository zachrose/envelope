module ApplicationHelper
  def homepage_or_my_events_path
    current_admin ? my_events_path : root_path
  end
end
