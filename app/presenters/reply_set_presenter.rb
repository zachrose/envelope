class ReplySetPresenter
  
  # presents a party from the perspective of a guest
  def initialize(reply_set)
    @reply_set = reply_set
  end
  
  def events
    @reply_set.replies.map { |reply|
      reply.event
    }.uniq
  end
  
  def any_attending?(event)
    @reply_set.select_where({event: event, status: true}).any?
  end
  
  def any_not_attending?(event)
    @reply_set.select_where({event: event, status: false}).any?
  end
  
  def all_empty_replies?
    if @reply_set.select_where({status: true}).any?
      return false
    elsif @reply_set.select_where({status: false}).any?
      return false
    end
    return true
  end
  
  def translation_key(event, coming)
    
    status = (coming == :coming) ? true : false
    replies = @reply_set.select_where({event: event, status: status})
    guests = replies.map(&:guest).uniq
    
    if has_focused_member?(guests)
      if (guests.length == 1)
        "#{coming}.focus"
      elsif (guests.length == 2)
        if has_member_with_unknown_name?(guests)
          "#{coming}.focus_and_unknown_guest"
        else
          "#{coming}.focus_and_guest"
        end
      elsif (guests.length > 2)
        if has_members_with_unknown_name?(guests)
          "#{coming}.focus_and_unknown_guests"
        else
          "#{coming}.focus_and_guests"
        end
      end
    else
      "#{coming}.guest_or_guests"
    end
  end
  
  def guest_sentence(event, coming)
    status = (coming == :coming) ? true : false
    replies = @reply_set.select_where({event: event, status: status})
    guests = replies.map(&:guest).uniq
    guests = guests.map do |g| Guest.find(g.id) end # reload to get newly-created names
    familiar_names = without_focused(guests).map(&:familiar_name).compact.reject(&:blank?)
    familiar_names.reverse.to_sentence
  end
  
  private
  
  def is_focused(guest)
    guest.id == @reply_set.guest.id
  end
  
  def has_focused_member?(guests)
    guests.any?{ |guest| is_focused guest }
  end
  
  def without_focused(guests)
    guests.reject{ |guest| is_focused guest }
  end
  
  def has_member_with_unknown_name?(guests)
    result = without_focused(guests).one?{ |guest|
      any_names = @reply_set.replies.where(guest: guest).map(&:unknown_name).compact
      guest.unknown_name && any_names.none?
    }
    return result
  end
  
  def has_members_with_unknown_name?(guests)
    result = without_focused(guests).many?{ |guest|
      any_names = @reply_set.replies.where(guest: guest).map(&:unknown_name).compact
      guest.unknown_name && any_names.none?
    }
    return result
  end
  
end