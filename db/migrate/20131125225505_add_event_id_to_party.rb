class AddEventIdToParty < ActiveRecord::Migration
  def change
    remove_column :events, :admin_id
    add_column :parties, :event_id, :integer
  end
end
