class MergeEventsAndCompanionEvents < ActiveRecord::Migration
  
  def up
    add_column :events, :main_event_id, :integer
    drop_table :companion_events
  end
  
  def down
    remove_column :events, :main_event_id
    create_table :companion_events do |t|
      t.integer  "event_id"
      t.string   "kind"
      t.text     "info"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
  
end
