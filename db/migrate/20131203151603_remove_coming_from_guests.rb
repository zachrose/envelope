class RemoveComingFromGuests < ActiveRecord::Migration
  def change
    remove_column :guests, :coming, :boolean
  end
end
