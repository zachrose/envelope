class AddUnknownNameFlag < ActiveRecord::Migration
  def change
    add_column :guests, :unknown_name, :boolean
  end
end
