class CreateUnexpectedGuestMessages < ActiveRecord::Migration
  def change
    create_table :unexpected_guest_messages do |t|
      t.integer :main_event_id
      t.string :first_name
      t.string :last_name
      t.text :body
    end
  end
end
