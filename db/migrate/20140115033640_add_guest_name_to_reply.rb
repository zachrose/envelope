class AddGuestNameToReply < ActiveRecord::Migration
  def change
    add_column :replies, :unknown_name, :string
  end
end