class RenamePartiesEventIdToPartiesMainEventId < ActiveRecord::Migration
  def change
    rename_column :parties, :event_id, :main_event_id
  end
end
