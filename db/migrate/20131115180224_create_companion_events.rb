class CreateCompanionEvents < ActiveRecord::Migration
  def change
    create_table :companion_events do |t|
      t.integer :event_id
      t.string :kind
      t.text :info

      t.timestamps
    end
  end
end
