class CreateInvitation < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.integer :party_id
      t.integer :event_id
    end
  end
end
