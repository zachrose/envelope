class CreateParties < ActiveRecord::Migration
  def change
    add_column :guests, :party_id, :integer
    create_table :parties do |t|
      t.timestamps
    end
  end
end
