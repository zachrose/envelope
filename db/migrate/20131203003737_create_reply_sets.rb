class CreateReplySets < ActiveRecord::Migration
  def change
    create_table :reply_sets do |t|
      t.integer :guest_id
      t.timestamps
    end
    add_column :replies, :reply_set_id, :integer
  end
end
