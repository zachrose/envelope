class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :short_name
      t.string :kind

      t.timestamps
    end
  end
end
