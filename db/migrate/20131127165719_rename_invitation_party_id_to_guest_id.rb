class RenameInvitationPartyIdToGuestId < ActiveRecord::Migration
  def change
    rename_column :invitations, :party_id, :guest_id
  end
end
