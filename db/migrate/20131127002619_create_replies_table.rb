class CreateRepliesTable < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.integer :guest_id
      t.integer :event_id
      t.boolean :status
    end
  end
end
