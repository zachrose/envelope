# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# short_name = "example"
# admin = Admin.create!({email: "admin@example.com", password: "password", password_confirmation: "password", first_event_short_name: short_name})
# main_event = MainEvent.find_by_short_name(short_name)
# main_event.update({kind: "wedding", info: "The wedding will be at Minnie's house at 1:00 PM. Minnie's house is in Mickey's Toontown, 1313 Disneyland Dr, Anaheim, CA 92802."})
# companion_event = CompanionEvent.create!({main_event: main_event, kind: "rehearsal dinner", info: "companion_event the main_event it's the companion_event-party. The party will be at Mickey's house at 2:00 PM. Mickey's house is in Mickey's Toontown, 1313 Disneyland Dr, Anaheim, CA 92802."})
# 
# Party.create([
#   { guests_attributes: [
#     {first_name: "Mickey", last_name: "Mouse"},
#     {first_name: "Minnie", last_name: "Mouse"}
#   ]},
#   { guests_attributes: [
#     {first_name: "Donald", last_name: "Duck"},
#     {first_name: "Daisy", last_name: "Duck"}
#   ]}
# ])
# 
# Guest.create([
#   {first_name: "Clarabelle", last_name: "Cow"},
#   {first_name: "Phantom", last_name: "Blot"}
# ])
# 
# Guest.all.each do |guest|
#   guest.invite_to(main_event, companion_event)
# end
# 
# Guest.where("last_name != ?", "Blot").each do |guest|
#   guest.invite_to(main_event, companion_event)
# end