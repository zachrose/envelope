# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140209222400) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "events", force: true do |t|
    t.string   "short_name"
    t.string   "kind"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "info"
    t.integer  "admin_id"
    t.integer  "main_event_id"
    t.string   "type"
  end

  create_table "guests", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "party_id"
    t.boolean  "unknown_name"
  end

  create_table "invitations", force: true do |t|
    t.integer "guest_id"
    t.integer "event_id"
  end

  create_table "parties", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "main_event_id"
  end

  create_table "replies", force: true do |t|
    t.integer "guest_id"
    t.integer "event_id"
    t.boolean "status"
    t.integer "reply_set_id"
    t.string  "unknown_name"
  end

  create_table "reply_sets", force: true do |t|
    t.integer  "guest_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "support_requests", force: true do |t|
    t.string "email"
    t.text   "body"
  end

  create_table "unexpected_guest_messages", force: true do |t|
    t.integer "main_event_id"
    t.string  "first_name"
    t.string  "last_name"
    t.text    "body"
  end

end
