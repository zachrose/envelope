# Be sure to restart your server when you modify this file.

path = File.join(Rails.root, 'app', 'views', 'tour', '*')
count = Dir[path].count { |file| File.file?(file) }
Envelope::Application.config.tour_pages_count = count
