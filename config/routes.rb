Envelope::Application.routes.draw do
  
  root 'homepage#index'
  
  get 'about',   to: 'homepage#about', as: :about
  get 'privacy', to: 'homepage#privacy', as: :privacy
  get 'errors', to: 'homepage#error'
  get 'errors/:status', to: 'homepage#error'
  get 'tour/:page', to: 'homepage#tour', as: :tour
  get 'tour', to: 'homepage#tour'
  
  get 'help',    to: 'support_requests#new', as: :help
  post 'help',   to: 'support_requests#create', as: :support_request_create
  
  get  'my_events', to: 'events#index', as: :my_events
  get 'my_events/:short_name', to: 'events#show', as: :main_event
  put  'my_events/:short_name', to: 'events#update'
  patch  'my_events/:short_name', to: 'events#update'
  
  delete 'events/:id', to: 'events#destroy', as: :events_destroy
  
  get 'my_events/:short_name/guests', to: 'guests#index', as: :guests_show
  post 'my_events/:short_name/parties', to: 'parties#create', as: :party_create
  post 'my_events/:short_name/guests', to: 'parties#create', as: :guests_create
  get 'my_events/:short_name/messages', to: 'messages#index', as: :messages_show
  
  get 'my_events/:short_name/guests/new', to: 'parties#new_guest', as: :guest_new
  get 'my_events/:short_name/parties/new', to: 'parties#new', as: :party_new
  get 'my_events/:short_name/parties/:id/edit', to: 'parties#edit', as: :party_edit
  put  'my_events/:short_name/parties/:id', to: 'parties#update'
  patch  'my_events/:short_name/parties/:id', to: 'parties#update', as: :party_patch

  resources :replies, only: [:create]

  devise_for :admins, path: '', path_names: {
    sign_in:  'login',
    sign_out: 'logout',
    sign_up:  'register', as: :new_user_registration
  }
  
  get ':event', to: 'reply_sets#hello', as: :site
  post ':event/attendance', to: 'reply_sets#new'
  post ':event/confirmation', to: 'reply_sets#create'
  post ':event/messages',   to: 'messages#create', as: :message_create
  
end