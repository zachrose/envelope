Then(/^I should be welcomed as "(.*?)"$/) do |name|
  page.should have_text I18n.t('hi_guest', name: name)
end

Then(/^I should be told that name could not be found$/) do
  page.should have_text I18n.t 'name_not_found'
end

Then(/^I should be asked if I'm coming to the "(.*?)"$/) do |kind|
  page.should have_text "Are you coming to the #{kind}?"
end

Then(/^I should be asked if "(.*?)" is coming to the "(.*?)"$/) do |guest, kind|
  page.should have_text "Is #{guest} coming to the #{kind}?"
end

Given(/^I am on the event page for "(.*?)"$/) do |name|
  visit '/'+name
end

Given(/^I have said I am (not\s)?coming to the "(.*?)"$/) do |negative, short_name|
  coming = negative.nil? ? "Yes" : "No"
  label = find('label', text: "Are you coming to the #{short_name}?")
  parent = label.find(:xpath, '..')
  parent.find_field(coming).click
end

Given(/^I have said that "(.*?)" is (not\s)?coming to the "(.*?)"$/) do |familiar_name, negative, short_name|
  coming = negative.nil? ? "Yes" : "No"
  label = find('label', text: "Is #{familiar_name} coming to the #{short_name}?")
  parent = label.find(:xpath, '..')
  parent.find_field(coming).click
end

Then(/^I should be asked if I'm bringing someone\.$/) do
  page.should have_text "Are you bringing someone?"
end

Then(/^when I say yes I'm bringing someone$/) do
  label = find 'label', text: "Are you bringing someone?"
  parent = label.find(:xpath, '..')
  parent.find_field('Yes').click
end

Then(/^when I say no I'm not bringing someone$/) do
  label = find 'label', text: "Are you bringing someone?"
  parent = label.find(:xpath, '..')
  parent.find_field('No').click
end

When(/^I type in my guests name as "(.*?)"$/) do |name|
  fill_in 'what_their_name', with: name
end

Then(/^I should be asked for that person's name\.$/) do
  page.should have_text "What is this person's name?"
end

Then(/^I shouldn't be asked for that person's name\.$/) do
  page.should_not have_text "What is this person's name?"
end

When(/^I sign in as "(.*?)", "(.*?)"$/) do |last_name, first_name|
  step "I fill in my first_name as \"#{first_name}\""
  step "I fill in my last_name as \"#{last_name}\""
  step "I submit the form"
end

Then(/^I should be prompted to leave a message$/) do
  label = page.find("label")
  label.text.should include "Please leave us a message"
  textarea = find(:xpath, "//textarea[@name='unexpected_guest_message[body]']")
  textarea.should be
end
