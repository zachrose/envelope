Given(/^there is an event site for a "(.*?)" at "(.*?)"$/) do |kind, short_name|
  MainEvent.create! short_name: short_name, kind: kind
end

Given(/^there is a companion "(.*?)" at "(.*?)"$/) do |kind, short_name|
  CompanionEvent.create! kind: kind, main_event: Event.find_by_short_name(short_name)
end

Given(/^the site "(.*?)" has a guest named "(.*?)"$/) do |short_name, guest_name|
  event = MainEvent.find_by_short_name short_name
  (head, *rest) = guest_name.split(', ')
  Party.create!({
    guests: [Guest.create!(last_name: head, first_name: rest.join(', '))],
    main_event: event
  })
end

Given(/^a guest named "(.*?)" is added to the party of "(.*?)"$/) do |name1, name2|  
  guest2 = Guest.find_by_name name2
  party = guest2.party
  (head, *rest) = name1.split(', ')
  guest2.party.guests.create! last_name: head, first_name: rest.join(', ')
end

Given(/^"(.*?)" is invited to the "(.*?)" at "(.*?)"$/) do |name, kind, short_name|
  main = Event.find_by_short_name short_name
  companion = main.companion_events.where(kind: kind).first
  guest = Guest.find_by_name name
  guest.invite_to companion
end

When(/^an additional guest is added to the party of "(.*?)"$/) do |name|
  party = Guest.find_by_name(name).party
  party.guests.create! unknown_name: true
end

Given(/^somebody named "(.*?)" "(.*?)" says "(.*?)" at "(.*?)"$/) do |first_name, last_name, body, short_name|
  UnexpectedGuestMessage.create!({
    first_name: first_name,
    last_name: last_name,
    body: body,
    main_event: MainEvent.find_by_short_name(short_name)})
end
