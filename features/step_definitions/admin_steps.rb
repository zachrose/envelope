Given(/^there is an admin with email "(.*?)", password "(.*?)", and first event short name "(.*?)"$/) do | email, password, first_event_short_name |
  Admin.create! email: email, password: password, first_event_short_name: first_event_short_name
end

Given(/^I am on the login page$/) do
  visit '/login'
end

Given(/^an admin exists with email "(.*?)" and event "(.*?)"$/) do | email, event |
  Admin.create!({
    email: email,
    first_event_short_name: event,
    password: "password"
  }) 
end

Given(/^the event "(.*?)" has a companion event which is a "(.*?)"$/) do |short_name, kind|
  Event.find_by_short_name(short_name).companion_events.create! kind: kind
end


Given(/^the admin "(.*?)" is logged in$/) do | email |
  visit '/login'
  fill_in 'admin_email', with: email
  fill_in 'admin_password', with: "password"
  find('input[type="submit"]').click
end

Given(/^"(.*?)" is on the management page for "(.*?)"$/) do |email, short_name|
  visit "/my_events/#{short_name}"
end

When(/^I pick my event kind as "(.*?)"$/) do | kind |
  within_fieldset "main_event" do
    select kind, from: 'main_event[kind]'
  end
end

When(/^I enter my event kind as "(.*?)"$/) do | kind |
  within_fieldset "main_event" do
    select 'other', from: 'main_event[kind]'
    fill_in "main_event[other_kind]", with: kind
  end
end


When(/^I fill in the informational text as "(.*?)"$/) do | text |
  within_fieldset "main_event" do
    # fill_in('event_info', with: text)
    fill_in_redactor :in => ".field.info", :with => text
  end
end

When(/^I click to add a companion event$/) do
  find('.add_companion_event').click
end

When(/^I click the "(.*?)" button$/) do |name|
  click_button name
end

Then(/^I should see that the main event is an? "(.*?)"$/) do | kind |
  within_fieldset "main_event" do
    value = find_field('main_event[kind]').value
    if value == '__custom__'
      value = find_field('main_event[other_kind]').value
    end
    value.should == kind
  end
end

Then(/^I should see that there is a companion event which is an "(.*?)"$/) do |kind|
  within_fieldset "Companion Event" do
    value = find('select').value
    if value == '__custom__'
      value = find('.other').value
    end
    value.should == kind
  end
end

Then(/^I should see that there is not a companion event$/) do
  page.should have_no_selector(:xpath, "//fieldset[@class='companion_event']")
end

Then(/^I should see that the "(.*?)" has informational text of "(.*?)"$/) do |kind, type|
  within_fieldset "Companion Event" do
    text.should == text
  end
end

Then(/^I should see that the main event says "(.*?)"$/) do | text |
  within_fieldset "main_event" do
    text.should == text
  end
end


When(/^I enter my companion event kind as "(.*?)"$/) do | kind |
  within_fieldset "Companion Event" do
    select 'other'
    find(:css, "input.other").set(kind)
  end
end

When(/^I fill in the companion informational text as "(.*?)"$/) do |text|
  within_fieldset "Companion Event" do
    fill_in_redactor :in => ".field.info", :with => text
  end
end

When(/^I click the "(.*?)" tab$/) do |text|
  within('#nav') do
    find('a', :text => text).click
  end
end

When(/^I should see a guest named "(.*?)"$/) do |name|
  page.should have_css('td.name', text: name)
end

Then(/^I should not see a guest named "(.*?)"$/) do |name|
  page.should have_no_selector(:xpath, "//td[@class='name' and text()='#{name}']")
end


When(/^I should see that "(.*?)" and "(.*?)" are in the same party$/) do |name1, name2|
  e1 = page.find :css, 'tbody tr td.name', text: name1
  e2 = page.find :css, 'tbody tr td.name', text: name2
  tbody1 = e1.find :xpath, '../..'
  tbody2 = e2.find :xpath, '../..'
  tbody1.should == tbody2
end


Then(/^I should see that "(.*?)" is invited to the main event$/) do |name|
  name_element = page.find :css, 'tbody tr td.name', text: name
  status_el = name_element.find :xpath, '../td[@class="status"][1]/span[@class="status"][1]'
  status_el.should have_text('Invited')
end

Then(/^I should see that "(.*?)" is invited to the companion event$/) do |name|
  name_element = page.find :css, 'tbody tr td.name', text: name
  status_el = name_element.find :xpath, '../td[@class="status"][2]/span[@class="status"][1]'
  status_el.should have_text('Invited')
end


When(/^I click "Add another guest"$/) do
  find('.add_another_guest').click
end

When(/^I fill in "(.*?)" as "(.*?)" for the "(\d*).*" guest$/) do |value, field, n|
  within page.all('fieldset.guest')[n.to_i - 1] do
    fill_in field, with: value
  end
end

When(/^I go to edit the party of "(.*?)"$/) do |name|
  guest_el = page.find :css, 'tbody tr td.name', text: name
  party_el = guest_el.find(:xpath, '../../tr[1]')
  party_el.find('.edit a').click
end

When(/^I mark "(.*?)" as "(.*?)" to the main event$/) do |name, status|
  guest_el = page.find :css, 'tbody tr.guest', text: name
  main_status_el = guest_el.find :xpath, 'td[@class="status"][1]'
  main_status_el.find('.edit a').click
  main_status_el.find('button', text: status).click
end

Then(/^I should see that "(.*?)" has a status of "(.*?)" for main event$/) do |name, status|
  guest_el = page.find :css, 'tbody tr.guest', text: name
  main_status_el = guest_el.find :xpath, 'td[@class="status"][1]/span[@class="status"]'
  main_status_el.should have_content status
end

Then(/^I should see that "(.*?)" has an additional guest$/) do |name|
  guest_el = page.find :css, 'tbody tr.guest', text: name
  guest_el.find(:xpath, '../tr[last()]').text.should have_content "+ Guest"
end

When(/^I click to remove a companion event$/) do
  companion_event_el = page.find(:css, '.companion_event')
  remove_el = companion_event_el.find(:xpath, 'legend//span[@class="remove link"][1]')
  remove_el.click
end

Then(/^I should see a message from "(.*?)" saying "(.*?)"$/) do |full_name, body|
  name_el = page.find :xpath, "//li[@class='message']/h3[text()='#{full_name}']"
  body_el = page.find :xpath, "//li[@class='message']/p[@class='body' and text()='#{body}']"
  name_el.should be
  body_el.should be
end

