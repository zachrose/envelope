Given(/^I am on the homepage$/) do
  visit '/'
end

When(/^I fill in my (.*?) as "(.*?)"$/) do |field, value|
  fill_in field, with: value
end

When(/^I fill in "(.*?)" as "(.*?)"$/) do |field, value|
  fill_in field, with: value
end

# clean this up
When(/^I submit the form$/) do
  click_button 'submit'
end

When(/^I click submit$/) do
  find('input[type="submit"]').click
end

Then(/^I should see "(.*?)"$/) do |text|
  page.should have_text text
end

When(/^I follow "(.*?)"$/) do |link_text|
  click_link link_text
end

When(/^I check "(.*?)"$/) do |checkbox_label_or_id|
  page.check(checkbox_label_or_id)
end

When(/^I confirm the browser dialog$/) do
  true
  # poltergeist always confirms confirm dialogs
end

When(/^I refresh the page$/) do
  visit(current_path)
end