Feature: Single guest is asked about a plus one
  In order to feel welcome to bring a date
  As a guest of an event
  I want to be asked if I'm bringing someone, and what that person's name is

Background:
  Given there is an event site for a "party" at "our-party"
  And the site "our-party" has a guest named "Connor, John"
  And an additional guest is added to the party of "John Connor"
  And I am on the event page for "our-party"
  And I fill in my first_name as "John"
  And I fill in my last_name as "Connor"

Scenario: Guest is bringing a guest
  When I submit the form
  Then I should be welcomed as "John"
  And I should be asked if I'm coming to the "party"
  And I have said I am coming to the "party"
  And I should be asked if I'm bringing someone.
  And when I say yes I'm bringing someone
  Then I should be asked for that person's name.
  When I type in my guests name as "Uncle Bob"
  And I click submit
  Then I should see "We look forward to seeing you and Uncle Bob at the party."

Scenario: Guest is bringing a guest without a name
  When I submit the form
  Then I should be welcomed as "John"
  And I should be asked if I'm coming to the "party"
  And I have said I am coming to the "party"
  And I should be asked if I'm bringing someone.
  And when I say yes I'm bringing someone
  Then I should be asked for that person's name.
  And I click submit
  Then I should see "We look forward to seeing you and your guest at the party."

Scenario: Guest is not bringing a guest
  When I submit the form
  Then I should be welcomed as "John"
  And I should be asked if I'm coming to the "party"
  And I have said I am coming to the "party"
  And I should be asked if I'm bringing someone.
  And when I say no I'm not bringing someone
  Then I shouldn't be asked for that person's name.
  And I click submit
  Then I should see "We look forward to seeing you at the party."

Scenario: Attendance at companion event is assumed
  When there is a companion "after-party" at "our-party"
  And "John Connor" is invited to the "after-party" at "our-party"
  And I submit the form
  Then I should be welcomed as "John"
  And I should be asked if I'm coming to the "after-party"
  And I have said I am coming to the "party"
  And I have said I am coming to the "after-party"
  And I should be asked if I'm bringing someone.
  And when I say yes I'm bringing someone
  Then I should be asked for that person's name.
  When I type in my guests name as "Uncle Bob"
  And I click submit
  Then I should see "We look forward to seeing you and Uncle Bob at the party."
  And I should see "We look forward to seeing you and Uncle Bob at the after-party."

