Feature: Guest is recognized
  In order to feel expected
  As a guest of an event
  I want to be recognized by my familiar name, and asked about guests in my party
  
Background:
  Given there is an event site for a "wedding" at "our-wedding"
  And the site "our-wedding" has a guest named "Hendrix, Pat"
  And a guest named "Hendrix, Joe" is added to the party of "Pat Hendrix"

Scenario: Guest is recognized
  When I am on the event page for "our-wedding"
  And I fill in my first_name as "Joe"
  And I fill in my last_name as "Hendrix"
  And I submit the form
  Then I should be welcomed as "Joe"

Scenario: Guest is asked about members of party
  When I am on the event page for "our-wedding"
  And I fill in my first_name as "Joe"
  And I fill in my last_name as "Hendrix"
  And I submit the form
  Then I should be asked if I'm coming to the "wedding"
  And I should be asked if "Pat" is coming to the "wedding"