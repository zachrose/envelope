Feature: Guest reply to event
  In order to be polite
  As a guest of an event
  I want to reply for myself and any people in my party

Background:
  Given there is an event site for a "party" at "site"
  And the site "site" has a guest named "Hendrix, Joe"
  And a guest named "Hendrix, Pat" is added to the party of "Joe Hendrix"
  And the site "site" has a guest named "Frank"
  And I am on the event page for "site"

Scenario: Guest is coming
  When I fill in my first_name as "Frank"
  And I click submit
  And I have said I am coming to the "party"
  When I click submit
  Then I should see "We look forward to seeing you at the party."
  
Scenario: Guest is not coming
  When I fill in my first_name as "Frank"
  And I click submit
  And I have said I am not coming to the "party"
  When I click submit
  Then I should see "We're sorry you can't make it to the party."

Scenario: Guest declines for self, accepts for member of party
  When I fill in my first_name as "Joe"
  And I fill in my last_name as "Hendrix"
  And I click submit
  And I have said I am not coming to the "party"
  And I have said that "Pat" is coming to the "party"
  When I click submit
  Then I should see "The attendance of Pat at the party has been confirmed."
  And I should see "We're sorry you can't make it."

Scenario: Guest declines for all
  When I fill in my first_name as "Joe"
  And I fill in my last_name as "Hendrix"
  And I click submit
  And I have said I am not coming to the "party"
  And I have said that "Pat" is not coming to the "party"
  When I click submit
  Then I should see "We're sorry that you and Pat can't make it to the party."

Scenario: Guest accepts for all
  When I fill in my first_name as "Joe"
  And I fill in my last_name as "Hendrix"
  And I click submit
  And I have said I am coming to the "party"
  And I have said that "Pat" is coming to the "party"
  When I click submit
  Then I should see "We look forward to seeing you and Pat at the party."