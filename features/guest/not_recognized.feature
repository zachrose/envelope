Feature: Unexpected guests can leave messages
  In order to feel expected, even when I'm not
  As a guest of an event who can't type in their name right
  I should be recognized by whatever name I give, and asked to leave a message
  
Background:
  Given there is an event site for a "wedding" at "our-wedding"

Scenario: Unexpected guest is fake recognized
  When I am on the event page for "our-wedding"
  And I fill in my first_name as "Wraith"
  And I fill in my last_name as "Incandenza"
  And I click submit
  Then I should be welcomed as "Wraith"
  And I should be prompted to leave a message

Scenario: Unexpected guest leaves a message
  When I am on the event page for "our-wedding"
  And I sign in as "Incandenza", "Wraith"
  And I fill in "body" as "I'm coming"
  And I click submit
  Then I should see "Thank you"