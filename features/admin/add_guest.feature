Feature: Admin can add a guest to an event
  As an admin
  In order to get RSVPs for an event
  I want to add individual an guest to my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the event "mygreatparty" has a companion event which is a "afterparty"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"
  When I click the "Guests" tab

Scenario: Admin adds a party with a single guest
  And I follow "Add a Guest"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I click submit
  Then I should see a guest named "Michael Jackson"

Scenario: Admin adds a guest and invites that party to a companion event
  And I follow "Add a Guest"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I check "Invite to afterparty"
  And I click submit
  Then I should see a guest named "Michael Jackson"
  And I should see that "Michael Jackson" is invited to the companion event

Scenario: Admin adds a guest with a plus one
  And I follow "Add a Guest"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I check "Ask if they're bringing someone"
  And I click submit
  Then I should see a guest named "Michael Jackson"
  And I should see that "Michael Jackson" has an additional guest