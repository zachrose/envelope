Feature: Admin can add a party of guests to an event
  As an admin
  In order to get RSVPs for an event
  I want to add parties of guests to my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the event "mygreatparty" has a companion event which is a "rehearsal dinner"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin adds a party with a single guest
  When I click the "Guests" tab
  And I follow "Add a Party of Guests"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I click submit
  Then I should see a guest named "Michael Jackson"

Scenario: Admin adds a party and invites that party to a companion event
  When I click the "Guests" tab
  And I follow "Add a Party of Guests"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I check "Invite to rehearsal dinner"
  And I click submit
  Then I should see a guest named "Michael Jackson"
  And I should see that "Michael Jackson" is invited to the companion event

Scenario: Admin adds a party with multiple guests
  When I click the "Guests" tab
  And I follow "Add a Party of Guests"
  And I fill in "First name" as "Michael"
  And I fill in "Last name" as "Jackson"
  And I click "Add another guest"
  And I fill in "Bubbles" as "First name" for the "2nd" guest
  And I click submit
  Then I should see a guest named "Michael Jackson"
  And I should see a guest named "Bubbles"