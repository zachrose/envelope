Feature: Admin can see a list of guests
  As an admin
  In order to plan my event
  I want to see a list of my guests

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the site "mygreatparty" has a guest named "Minnie"
  And a guest named "Mickey" is added to the party of "Minnie"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin sees guests
  When I click the "Guests" tab
  Then I should see a guest named "Mickey"
  And I should see a guest named "Minnie"

Scenario: Admin sees guests in the same party
  When I click the "Guests" tab
  Then I should see a guest named "Mickey"
  And I should see a guest named "Minnie"
  And I should see that "Mickey" and "Minnie" are in the same party