Feature: Admin can add a companion event
  As an admin
  In order to get RSVPs for and give information about a companion event
  I want to add a companion event to my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin adds afterparty
  When I click to add a companion event
  And I enter my companion event kind as "afterparty"
  And I fill in the companion informational text as "blah blah blah"
  And I click the "Save Changes" button
  Then I should see "updated"
  Then I should see that there is a companion event which is an "afterparty"
  And I should see that the "afterparty" has informational text of "blah blah blah"


