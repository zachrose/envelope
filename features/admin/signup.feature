Feature: Admin can register and have a new site
  In order to make my own event website
  As a curious visitor
  I want to get to register and have a new site

Scenario: Curious visitor signs up
  Given I am on the homepage
  When I follow "Register"
  And I fill in my admin_email as "zach@zachrose.biz"
  And I fill in my admin_password as "password"
  And I fill in my admin_first_event_short_name as "party"
  And I click submit
  Then I should see "My Events"
  And I should see "party"