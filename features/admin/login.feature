Feature: Admin can sign in and out
  In order to administrate my event
  As an administrator
  I want to get to my dashboard

Background:
  Given there is an admin with email "zach@zachrose.biz", password "password", and first event short name "mygreatevent"

Scenario: Admin is recognized
  Given I am on the homepage
  When I follow "Sign in"
  And I fill in my admin_email as "zach@zachrose.biz"
  And I fill in my admin_password as "password"
  And I click submit
  Then I should see "My Events"