Feature: Admin can give info about an event
  As an admin
  In order to provide a fluent interface
  I want to fill in details about my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin sets event kind and info text
  When I pick my event kind as "wedding"
  And I fill in the informational text as "blah blah blah"
  And I click the "Save Changes" button
  Then I should see "updated"
  Then I should see that the main event is a "wedding"
  And I should see that the main event says "blah blah blah"

Scenario: Admin sets custom event kind
  And I enter my event kind as "bar mitzvah"
  And I click the "Save Changes" button
  Then I should see "updated"
  Then I should see that the main event is a "bar mitzvah"