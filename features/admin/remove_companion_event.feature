Feature: Admin can remove a companion event
  As an admin
  In order to change my event, and to facilitate playing around
  I want to remove a companion event from my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And there is a companion "after-party" at "mygreatparty"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin removes afterparty
  When I click to remove a companion event
  And I confirm the browser dialog
  And I refresh the page
  Then I should see that there is not a companion event