Feature: Admin can reply for a guest
  As an admin
  In order to keep my guest list up to date
  I want to reply for guests that I know are coming.

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the event "mygreatparty" has a companion event which is a "rehearsal dinner"
  And the site "mygreatparty" has a guest named "Mouse, Minnie"
  And a guest named "Mouse, Mickey" is added to the party of "Minnie Mouse"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin marks guest as coming to an event
  When I click the "Guests" tab
  And I mark "Mickey Mouse" as "Confirmed" to the main event
  Then I should see that "Mickey Mouse" has a status of "Confirmed" for main event