Feature: Admin can see messages
  As an admin
  In order to be in touch with guests
  I want to read my messages

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And somebody named "Harry" "Henderson" says "Robble robble" at "mygreatparty"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin sees messages
  When I click the "Messages" tab
  Then I should see a message from "Harry Henderson" saying "Robble robble"