Feature: Admin can edit a party of guests
  As an admin
  In order to get RSVPs for an event
  I want to add parties of guests to my event

Background:
  Given an admin exists with email "bob@bob.bob" and event "mygreatparty"
  And the event "mygreatparty" has a companion event which is a "rehearsal dinner"
  And the site "mygreatparty" has a guest named "Mouse, Minnie"
  And a guest named "Mouse, Mickey" is added to the party of "Minnie Mouse"
  And the admin "bob@bob.bob" is logged in
  And "bob@bob.bob" is on the management page for "mygreatparty"

Scenario: Admin adds anther guest to party
  When I click the "Guests" tab
  And I go to edit the party of "Mickey Mouse"
  And I click "Add another guest"
  And I fill in "Pluto" as "First name" for the "3rd" guest
  And I click submit
  Then I should see a guest named "Pluto"